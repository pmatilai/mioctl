
mioctl
======

mioctl is an open-source command-line utility for configuring Mio X-series
devices from Linux and other operating systems not supported by iConnectivity.

Being a command-line tool means the approach to configuration is very
different from the official graphical tool, with emphasis on power
rather than pretty. To me the device looks more like router-firewall
on a MIDI network more than anything musical, so the command line interface
is modelled with that in mind, and making it possible to do things with
a single command things that would require dozens of mouse-clicks with
current Auracle.

When this project was started, there was no documentation for the X-series
configuration protocol, this was all reverse-engineered from analyzing
network traffic between Auracle and MioXL. As such, it is inevitable that
there will be errors and misunderstandings in the code.

As of March 2021, protocol documentation has silently been added to the
iConnectivity manuals page:
https://www.iconnectivity.com/s/Common-System-Exclusive-Commands-TNG.pdf

mioctl will naturally be updated to take advantage of the new knowledge,
eventually. USB host reservation is the first major improvement made
based on the official documentation.

Requirements
------------

- Python 3 (https://www.python.org/)
- Python MIDO library (https://github.com/mido/mido)

Developed and tested on Fedora 33-40, but the above is what matters.
On Fedora, `dnf install python3-mido` will install everything you need.
On other OS'es, device port names will differ but that is command-line
overridable.

Supported hardware
------------------

Tested with MioXL and MioXM running firmware 1.2.0 - 2.3.0.

The older Mio-devices use an entirely different protocol and I have no
means to support them as I have no access to such a device. However
there's another open-source project supporting them:
https://codeberg.org/dehnhardt/IConnConfig

Features
--------

As of version 0.2, the following is implemented:
- List, load and save (including rename) presets
- List, add and remove routes
- List device ports by their names
- List and set active remaps and filters
- View device information (model, serial, firmware version)
- View and set network configuration
- View and set USB Host reservations

I hope to eventually implement everything Auracle supports on these devices,
but this is a hobby project among many others so it might take time, and
due to the reverse-engineering nature there cannot be any promises.

Keep in mind this is a very early version with little in the way of sanity
and error checking, and is more likely to respond to errors with a Python
traceback than a coherent error message. It should not harm your device in
any way as it only uses commands that Auracle uses, but there cannot be any
guarantees.

Usage
-----

- `mioctl preset` shows currently loaded preset, add `--list` to list
  all preset by their names.
- `mioctl info` shows device model, serial and firmware version
- `mioctl net` shows current network configuration, `--dynamic` sets
  DHCP networking, `--static <ip> <network> <gateway>` sets statip ip.
  A reboot is required for the setting to take effect.
- `mioctl load [n]` loads a preset. If preset number is not specified,
  it reloads the current one.
- `mioctl device` allows remote device state changes:
  `--reboot` reboots to normal operation, `--bootloader` reboots to
  bootloader mode for firmware updates, and `--shutdown` shuts down
  the device.
- `mioctl save [n]` saves the current preset. If number is not specified,
  it saves on the current slot. `--name <presetname>` can be supplied
  to save with a different name.
- `mioctl port` lists device port numbers and names, `--name <name>`
  is used to rename ports. Multiple ports can be renamed at once, in
  which case an incrementing number is automatically used as a suffix.
- `mioctrl route` lists active routes.
  `mioctl route --add/--remove <source>:<target>` adds and removes routes,
  plain `mioctl route` lists current routing table. For example,
  `mioctl route --add 1,2:5` to add routes from ports 1 and 2 to port 5.
  Multiple port numbers are permitted on both source and target ports.
- `mioctl filter` lists currently active (input) filters,
  `--set [<event1>[,<event2>...]` can be used to set MIDI system message
  filters. To filter channel messages, specify `--channel <range>` in
  addition.
  For example, `mioctl filter --set SYSEX 1` enables SysEx message
  filtering on port 1,
  `mioctl filter --set CONTROL_CHANGE,PROGRAM_CHANGE --channel 1 31`
  enables CC and PC filtering on channel 1 of port 31,
  `mioctl filter --set NONE --channel 1:16 5` to clear all channel filters
  of port 5
  `mioctl filter --set ALL 6` to filter all system messages on port 6.

  The filterable system messages are:
  - `CLOCK`
  - `START`
  - `CONTINUE`
  - `STOP`
  - `QUARTER_FRAME`
  - `SONG_POSITION`
  - `SONG_SELECT`
  - `TUNE_REQUEST`
  - `ACTIVE_SENSE`
  - `RESET`
  - `SYSEX`

  The filterable channel messages are:
  - `NOTE_ON`
  - `NOTE_OFF`
  - `POLY_KEY`
  - `CONTROL_CHANGE`
  - `PROGRAM_CHANGE`
  - `CHANNEL_PRESSURE`
  - `PITCH_BEND`

  In addition to the above messages, `ALL` and `NONE` can be used to
  enable or disable filtering of all system or channel messages.

- `mioctl remap` lists currently active (input) channel remaps.
  `--set <event1>[,<event2>,...]` is be used to set the
  message types to remap, `--channel <from>:<to>` specifies the
  source and destination channels. Ports are specified as positional
  arguments as usual.
  For example, `mioctl remap --set NOTE_ON,NOTE_OFF --channel 1:16 35`
  remaps note on/off from channel 1 to channel 16, on port 35 (input).

  The remappable events are:
  - `NOTE_ON`
  - `NOTE_OFF`
  - `POLY_KEY`
  - `CONTROL_CHANGE`
  - `PROGRAM_CHANGE`
  - `CHANNEL_PRESSURE`
  - `PITCH_BEND`

  Like with `filter`, `ALL` and `NONE` are acceptable event values and
  can be used to enable/disable all remaps for a channel at once, eg.
  `mioctl remap --set ALL --channel 15:14 12` remaps all channel messages
  from channel 15 to channel 14, on port 12.

- `mioctl usbhost` lists current USB Host reservations (reserved but
  not connected in parentheses), `--unreserved`
  lists unreserved ports. Reserving new hosted devices is a two-step
  process: use `mioctl usbhost --connected` to enumerates connected
  devices per usbport, and then use
  `mioctl usbhost --reserve <usbport>:<midiport>` to reserve the
  desired usbport entry to the corresponding MIDI port. On multi-port
  devices, you can additionally specify the port number with
  `--portnum <n>` option. To prevent accidents, a host reservation must be
  explicitly released with `--release <midiport>` before reserving it
  for another device. Release and reserve can be combined on the same
  command and both accept multiple arguments.

Most port-related commands accept ports as positional arguments.
Port and channel ranges can be specified as comma-separated combinations
of individual numbers and numeric ranges, such as: `1,5,8-19`.
`filter`, `remap` and `route` by default only show ports with active
configuration, and accept `--all` to include all ports.

By default it will look for a MIDI port name containing 'mioX' which
should find it when connected via USB but this can be overridden with
`--midiport <name>` for access through any OS MIDI port that is somehow
connected to the device (be it DIN or otherwise)

Firmware upgrade
----------------

Firmware upgrade is not supported by `mioctl` at this time, however you
can easily do this on Linux with the `aplaymidi` utility. Connect your
computer to the USB DAW port on the Mio, boot up the
device into bootloader mode (hold button on powerup for 4-5s on powerup),
then use `aplaymidi -l` to discover the port of the Mio (only one port
is visible in bootloader mode, called "mioXL Loader" or so), and "play"
the firmware .mid file you've downloaded file into that port, eg.
MioXL 1.3.1 firmware upgrade is something like this:
`aplaymidi -p "mioXL Loader" mioXL_DFU_131_20210120.mid`
You should see a lot of traffic on the USB port, once it stops and
the command exits successfully, power cycle the Mio and it should now boot
into the new firmware.
